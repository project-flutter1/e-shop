import 'package:e_shop/forgot_password/forgot_password_srceen.dart';
import 'package:e_shop/sign_in/sign_in_screen.dart';
import 'package:e_shop/splash_screen.dart';
import 'package:flutter/widgets.dart';

final Map<String, WidgetBuilder> routes = {

  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),

};