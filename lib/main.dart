import 'package:e_shop/constants.dart';
import 'package:e_shop/routs.dart';
import 'package:e_shop/theme.dart';
import 'package:flutter/material.dart';
import 'package:e_shop/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',

      theme: themeData()
      ,
      //home: SplashScreen(),
      initialRoute: SplashScreen.routeName,
      routes: routes,
    );
  }
}


