import 'package:e_shop/DefaultButton.dart';
import 'package:e_shop/constants.dart';
import 'package:e_shop/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  String email ;
  String password;
  bool remember = false;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Column(
          children: [
            buildTextFormFieldEmail(),
            SizedBox(
              height: getProportionateScreenHeigh(20),
            ),
            buildTextFormFieldPassword(),
            SizedBox(
              height: getProportionateScreenHeigh(20),
            ),
            FormError(errors: errors),
            SizedBox(
              height: getProportionateScreenHeigh(20),
            ),

            Row(children: [
              Checkbox(value: remember,
                  activeColor: kPrimaryColor,
                  onChanged: (value){

                setState(() {
                  remember = value;
                });

              }),
              Text("Remember me"),
              Spacer(),
              Text("Forgot Password", style: TextStyle(decoration: TextDecoration.underline),)

            ],),



            DefaultButton(
              text: "Continue",
              press: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                }
              },
            )
          ],
        ),
      ),
    );
  }

  TextFormField buildTextFormFieldPassword()
  {


    return TextFormField(
            obscureText: true,

        onSaved: (newValue) => password = newValue,
        onChanged: (value){
          if (value.isNotEmpty &&  errors.contains(kPassNullError)) {
            setState(() {
              errors.remove(kPassNullError /*"Please enter your email"*/);
            });
          }
          else if(value.length >= 8 && errors.contains(kShortPassError))
          {
            setState(() {
              errors.remove(kShortPassError);
            });
          }
          return null;
        },

        validator: (value) {
        if (value.isEmpty && ! errors.contains(kPassNullError)) {
          setState(() {
            errors.add(kPassNullError /*"Please enter your email"*/);
          });
        }
        else if(value.length < 8 && !errors.contains(kShortPassError))
        {
          setState(() {
            errors.add(kShortPassError);
          });
        }
        return null;
      },


      decoration: InputDecoration(
              labelText: "Password",
              hintText: "Enter your password",

              /*  suffixIcon: ,*/
              floatingLabelBehavior: FloatingLabelBehavior.always,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 42, vertical: 20),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: BorderSide(color: kTextColor),
                gapPadding: 10,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: BorderSide(color: kTextColor),
                gapPadding: 10,
              ),
            ),
          );
  }

  TextFormField buildTextFormFieldEmail() {
    return TextFormField(
            keyboardType: TextInputType.emailAddress,
            onSaved: (newValue) => email = newValue,
            onChanged: (value)
            {
              if (value.isNotEmpty && errors.contains(kEmailNullError)) {
                setState(() {
                  errors.remove(kEmailNullError /*"Please enter your email"*/);
                });
              }
              else if(emailValidatorRegExp.hasMatch(value) &&
                  errors.contains(kInvalidEmailError)) {
                setState(() {
                  errors.remove(kInvalidEmailError);
                });
              }
              return null;
            },

            validator: (value) {
              if (value.isEmpty && ! errors.contains(kEmailNullError)) {
                setState(() {
                  errors.add(kEmailNullError /*"Please enter your email"*/);
                });
              }
              else if(!emailValidatorRegExp.hasMatch(value) &&
              !errors.contains(kInvalidEmailError)) {
              setState(() {
                errors.add(kInvalidEmailError);
              });  
              }
              return null;
              },


            decoration: InputDecoration(
              labelText: "Email",
              hintText: "Enter your emile",
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: Icon( Icons.message),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 42, vertical: 20),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: BorderSide(color: kTextColor),
                gapPadding: 10,
              ),

              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(28),
                borderSide: BorderSide(color: kTextColor),
                gapPadding: 10,
              ),
            ),
          );
  }
}

class FormError extends StatelessWidget {
  const FormError({
    Key key,
    @required this.errors,
  }) : super(key: key);

  final List<String> errors;

  @override
  Widget build(BuildContext context) {
    return Column(
        children: List.generate(
            errors.length, (index) => FormErrorText(error: errors[index])));
  }

  Row FormErrorText({String error}) {
    return Row(
      children: [
             SvgPicture.asset(
        "assets/icons/error.svg",
        height: getProportionateScreenWidth(14),
        width: getProportionateScreenWidth(14),),
        SizedBox(
          width: getProportionateScreenWidth(14),
        ),
        Text(error)
      ],
    );
  }
}
