import 'package:e_shop/sign_in/body_sing_in.dart';
import 'package:flutter/material.dart';

class SignInScreen extends StatelessWidget {

  static  String routeName="/sign_in";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text("Sign In", ),
      ),
    body: BodySignIn(),
    );
  }
}
