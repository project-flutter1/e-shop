import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../size_config.dart';

class SocalCard extends StatelessWidget {


  const SocalCard({
    Key key, this.icon, this.preess,

  }):super(key: key);

  final String icon;
  final Function preess;

  @override
  Widget build(BuildContext context) {


    return  Container(
      margin: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10)),
      padding: EdgeInsets.all(getProportionateScreenWidth(12)),
      height: getProportionateScreenHeigh(40),
      width: getProportionateScreenWidth(40),
      decoration: BoxDecoration(
          color: Color(0xFFF5F6F9),
          shape: BoxShape.circle
      ), child: SvgPicture.asset(icon),
    );
  }
}
