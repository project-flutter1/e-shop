import 'package:e_shop/constants.dart';
import 'package:e_shop/forgot_password/forgot_password_srceen.dart';
import 'package:e_shop/sign_in/signform.dart';
import 'package:e_shop/sign_in/socal_card.dart';
import 'package:e_shop/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'no_account_text.dart';

class BodySignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child:
    SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),

        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: SizeConfig.screenHeight * 0.04 /* getProportionateScreenHeigh(20)*/),
              Text("Welcome Back",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: getProportionateScreenWidth(28),
                    fontWeight: FontWeight.bold
                ),),
              Text("Sign in with your email and password \nor continue with social media",
                textAlign: TextAlign.center,
              ),
              SizedBox(height: SizeConfig.screenHeight * 0.08 /* getProportionateScreenHeigh(20)*/),
              SignForm(),
              SizedBox(height: SizeConfig.screenHeight * 0.08 /* getProportionateScreenHeigh(20)*/),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SocalCard(
                    icon: "",
                    preess: (){},
                  ),
                  SocalCard(
                    icon: "",
                    preess: (){},
                  ),
                  SocalCard(
                    icon: "",
                    preess: (){},
                  ),
                ],
              ),
              SizedBox(height: getProportionateScreenHeigh(20),),
              NoAccountText(),

            ],
          ),
        ),
      ),
    )
    );
  }

}





