import 'package:flutter/material.dart';
import 'package:e_shop/constants.dart';
import 'package:e_shop/size_config.dart';
class SplashContent extends StatelessWidget {

  const SplashContent({
    Key  key,
    this.text,
    this.image
  }) : super(key: key);

  final String text,image;


  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Spacer(),
        Text(
          "SponT World",
          style: TextStyle(
              fontSize: getProportionateScreenWidth(36),
              color: kPrimaryColor,
              fontWeight: FontWeight.bold),
        ),
        Text(text,
          textAlign: TextAlign.center,),
        Spacer(flex: 2,),
        Image.asset(image,
          height: getProportionateScreenHeigh(265),
          width: getProportionateScreenWidth(235),
        )
      ],
    );
  }
}
