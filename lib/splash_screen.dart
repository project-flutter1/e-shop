import 'package:e_shop/body.dart';
import 'package:e_shop/size_config.dart';
import 'package:flutter/material.dart';


/*
class SplashScreen extends StatelessWidget*/

class SplashScreen extends StatelessWidget {

  static String routeName = "/spllash";

  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    return Scaffold(
      body: Body(),
    );
  }
}

