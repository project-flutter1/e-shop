import 'package:e_shop/size_config.dart';
import 'package:flutter/material.dart';

import '../constants.dart';

class ForgotBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(children: [
        Text("Forgot Password",style: TextStyle(
            fontSize: getProportionateScreenWidth(20),
            color: Colors.black,
            fontWeight: FontWeight.bold
        ),),

        Text("Please enter your email and we will send \n you a link to return to your account",
          textAlign: TextAlign.center,),
        ForgotPassForm(),

      ],)
    );
  }
}

class ForgotPassForm extends StatefulWidget {



  @override
  _ForgotPassFormState createState() => _ForgotPassFormState();
}

class _ForgotPassFormState extends State<ForgotPassForm> {

  final List<String> errors = [];

  String email ;

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
        TextFormField(
        keyboardType: TextInputType.emailAddress,
        onSaved: (newValue) => email = newValue,
        onChanged: (value)
        {
          if (value.isNotEmpty && errors.contains(kEmailNullError)) {
            setState(() {
              errors.remove(kEmailNullError /*"Please enter your email"*/);
            });
          }
          else if(emailValidatorRegExp.hasMatch(value) &&
              errors.contains(kInvalidEmailError)) {
            setState(() {
              errors.remove(kInvalidEmailError);
            });
          }
          return null;
        },

        validator: (value) {
          if (value.isEmpty && ! errors.contains(kEmailNullError)) {
            setState(() {
              errors.add(kEmailNullError /*"Please enter your email"*/);
            });
          }
          else if(!emailValidatorRegExp.hasMatch(value) &&
              !errors.contains(kInvalidEmailError)) {
            setState(() {
              errors.add(kInvalidEmailError);
            });
          }
          return null;
        },


        decoration: InputDecoration(
          labelText: "Email",
          hintText: "Enter your emile",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon( Icons.message),
          contentPadding:
          EdgeInsets.symmetric(horizontal: 42, vertical: 20),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(color: kTextColor),
            gapPadding: 10,
          ),

          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(28),
            borderSide: BorderSide(color: kTextColor),
            gapPadding: 10,
          ),
        ),
      )
        ],
      ),

    );
  }
}
