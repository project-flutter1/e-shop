
import 'package:e_shop/constants.dart';
import 'package:flutter/material.dart';

ThemeData themeData(){

  return
    ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primarySwatch: Colors.blue,

        appBarTheme: appBarTheme(),
        textTheme: textTheme(),

        visualDensity: VisualDensity.adaptivePlatformDensity,
    );
}

AppBarTheme appBarTheme(){
  return AppBarTheme(
      color: Colors.white,
      elevation: 0,
      brightness: Brightness.light,
      iconTheme: IconThemeData(color: Colors.black)
      ,
      textTheme: TextTheme(
        headline6: TextStyle(color: Color(0XFF8B8B8B),fontSize: 18 ),
      )

  );
}

TextTheme textTheme(){
  return TextTheme(
    bodyText1: TextStyle(color: kTextColor),
    bodyText2: TextStyle(color: kTextColor),
  );
}

