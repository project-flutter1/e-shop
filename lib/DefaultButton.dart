import 'package:e_shop/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'constants.dart';



class DefaultButton extends StatelessWidget {

  const DefaultButton ({
    Key key, this.text, this.press
  }) : super(key: key);

  final String text;
  final Function press;


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeigh(56),
      child: FlatButton(
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: kPrimaryColor,
          onPressed: press,
/* onPressed: onPressed(){},*/

          child: Text(
            text,
            style: TextStyle(color: Colors.white ,fontSize: getProportionateScreenWidth(18)),
          )),
    );
  }
}
